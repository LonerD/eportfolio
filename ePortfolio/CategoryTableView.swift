//
//  AlbumsTableView.swift
//  Mediaplatforma
//
//  Created by r00t on 13.03.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class CategoryTableView: UITableView {
    let categories = [("Студенти", UIImage(named: "student")), ("Викладачі", UIImage(named: "teacher"))]
    
    var headerView: UIView!
    
    var progressHeaderView: ((CGFloat) -> ())?
    
    var didSelectWith: ((Int) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerCells([CategoryCell()])
        delegate = self
        dataSource = self
        self.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    
    
    func setupHeaderWith(_ View: UIView) {
        self.headerView = View
        self.tableHeaderView = nil
        self.addSubview(self.headerView)
        self.contentInset = UIEdgeInsets(top: 200, left: 0, bottom: 0, right: 0)
        self.contentOffset = CGPoint(x: 0, y: -200)
        self.updateFrame()
    }
    
    
    fileprivate func updateFrame() {
        var headerRect = CGRect(x: 0, y: -200, width: UIScreen.main.bounds.width, height: 200)
        if  self.contentOffset.y < -200 {
            headerRect.origin.y = self.contentOffset.y
            headerRect.size.height = -self.contentOffset.y
        }
        self.headerView.frame = headerRect
    }
    
    
    func updateAnimate(updating: Bool) {
            self.setContentOffset(CGPoint(x: 0, y: updating ? -300 : -200), animated: true)
    }

}



extension CategoryTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let albumCell: CategoryCell = self.dequeueReusableCell(withCellClass: type(of: CategoryCell()), for: indexPath)
        
        albumCell.setupCellWith(categories[indexPath.row].0,
                                image: categories[indexPath.row].1!)
        
        return albumCell
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateFrame()
        if scrollView.contentOffset.y < 100 {
            self.progressHeaderView?(max(0, min(1 - (-scrollView.contentOffset.y / 100),1)))
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIScreen.main.bounds.height - 220) / CGFloat(categories.count)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didSelectWith?(indexPath.row)
        self.deselectRow(at: indexPath, animated: true)
    }
    
    
    
}
