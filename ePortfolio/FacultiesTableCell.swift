//
//  FacultiesTableCell.swift
//  ePortfolio
//
//  Created by r00t on 31.03.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class FacultiesTableCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!

    func setupCellWith(_ title: String) {
        self.titleLabel.text = title
    }
    
    override var reuseIdentifier: String? {
        return toStringType(type(of: self))
    }
    
}
