//
//  TeachersListVC.swift
//  ePortfolio
//
//  Created by r00t on 31.03.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class TeachersListVC: UIViewController {

    @IBOutlet weak var tableView: ListCollection!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.update(items: [(title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!),
                                      (title: "Гальперіна Любов Павлівна", image: UIImage(named: "thumbnail_1358791441")!)])
        
        self.tableView.didSelect = { item in
            self.performSegue(withIdentifier: "toDetail", sender: item)
        }
       
    }
    
    
    
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    

}
