//
//  StudentFaculties.swift
//  ePortfolio
//
//  Created by r00t on 31.03.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class StudentFacultiesVC: UIViewController {

    @IBOutlet weak var tableView: TitledTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Факультети"

        let faculties = ["Факультет мiжнародної економiки i менеджменту", "Юридичний факультет", "Факультет маркетингу", "Факультет управлiння персоналом, соціології та психології", "Облiково-економiчний факультет", "Факультет економiки АПК", "Фiнансово-економiчний факультет", "Факультет економiки та управлiння", "Факультет інформаційних систем і технологій"]
        
        self.tableView.setup(faculties)
        self.tableView.didSelectWith = { _ in
            self.performSegue(withIdentifier: "to", sender: nil)
        }
    }
    

}
