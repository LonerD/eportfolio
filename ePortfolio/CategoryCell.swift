//
//  AlbumCell.swift
//  Mediaplatforma
//
//  Created by r00t on 13.03.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    

    
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!


    func setupCellWith(_ Category: String, image: UIImage) {
        self.categoryImage.image = image
        self.categoryLabel.text = Category
        
        
    }
    
    override var reuseIdentifier: String? {
        return toStringType(type(of: self))
    }
    
}
