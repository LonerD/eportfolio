//
//  TeacherDepartmentsVC.swift
//  ePortfolio
//
//  Created by r00t on 31.03.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class TeacherDepartmentsVC: UIViewController {

    @IBOutlet weak var tableView: TitledTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Кафедри"

        let cources = ["Кафедра міжнародного менеджменту", "Кафедра міжнародної економіки", "Кафедра міжнародних фінансів", "Кафедра європейської інтеграції", "Кафедра іноземних мов і міжкультурної комунікації", "Кафедра німецької мови", "Кафедра української мови та літератури", "Кафедра міжнародної торгівлі", "Кафедра мiжнародного обліку і аудиту", "Кафедра теорії та історії права", "Кафедра конституційного та адміністративного права", "Кафедра кримінального права та процесу"]
        
        self.tableView.setup(cources)
        self.tableView.didSelectWith = { _ in
            self.performSegue(withIdentifier: "to", sender: nil)
        }
    }

  
}
