//
//  StudentCourseNumberVC.swift
//  ePortfolio
//
//  Created by r00t on 31.03.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class StudentCourseNumberVC: UIViewController {
    
    @IBOutlet weak var tableView: TitledTableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Курс"
        let cources = ["1 курс", "2 курс", "3 курс", "4 курс", "5 курс", "6 курс"]
        
        self.tableView.setup(cources)
        self.tableView.didSelectWith = { _ in
            self.performSegue(withIdentifier: "to", sender: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
