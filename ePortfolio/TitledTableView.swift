//
//  TiteledTableView.swift
//  ePortfolio
//
//  Created by r00t on 31.03.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class TitledTableView: UITableView {

    var titles: [String] = []
    
        var headerView: UIView!
        
        var progressHeaderView: ((CGFloat) -> ())?
        
        var didSelectWith: ((Int) -> ())?
        
        override func awakeFromNib() {
            super.awakeFromNib()
            self.registerCells([FacultiesTableCell()])
            delegate = self
            dataSource = self
            self.tableFooterView = UIView(frame: CGRect.zero)
        }
        
    func setup(_ With: [String]) {
        self.titles = With
    }
        
    }
    
    
    
    extension TitledTableView: UITableViewDelegate, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return titles.count
        }
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let titledCell: FacultiesTableCell = self.dequeueReusableCell(withCellClass: type(of: FacultiesTableCell()), for: indexPath)
            
            titledCell.setupCellWith(titles[indexPath.row])
            
            return titledCell
        }
        
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 100
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.didSelectWith?(indexPath.row)
            self.deselectRow(at: indexPath, animated: true)
        }
        
    }



