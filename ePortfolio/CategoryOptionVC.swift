//
//  ViewController.swift
//  ePortfolio
//
//  Created by r00t on 30.03.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class CategoryOptionVC: UIViewController {
    
    
    @IBOutlet weak var categoryTableView: CategoryTableView!
    
    var progress: CGFloat = 0 {
        didSet {
            self.setupWith(progress)
        }
    }
 

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.delegate = self
        self.setupWith(0)
        self.categoryTableView.setupHeaderWith(categoryTableView.tableHeaderView!)
         self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.categoryTableView.didSelectWith = { number in
            switch number {
            case 0:
                self.performSegue(withIdentifier: "toStudents", sender: nil)
            case 1:
                 self.performSegue(withIdentifier: "toTeachers", sender: nil)
            default:
                return
            }
        }
        
        self.categoryTableView.progressHeaderView = { progress in
            self.progress = progress
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(CategoryOptionVC.enterFromBackground), name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    func enterFromBackground() {
        self.setupWith(self.progress)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidBecomeActive, object: nil)
    }


    
    func setupWith(_ Progress: CGFloat) {
        UIApplication.shared.statusBarStyle = Progress > 0.2 ? .default : .lightContent
        self.navigationController?.navigationBar.alpha = Progress
    }
    
    func checkForBarUpdate(_ ViewController: UIViewController) {
        if ViewController == self {
            UIView.animate(withDuration: 0.3) {
                self.setupWith(self.progress)
            }
        } else {
            self.setupWith(1)
        }
    }
    


}


extension CategoryOptionVC: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        self.checkForBarUpdate(viewController)
    }
    
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        self.checkForBarUpdate(viewController)
    }
    
    
}

