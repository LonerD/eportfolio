//
//  ListCollection.swift
//  ePortfolio
//
//  Created by r00t on 03.04.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class ListCollection: UICollectionView {
    typealias itemType = (title: String, image: UIImage)
    
    var items: [itemType] = []
    
    var didSelect: ((itemType) -> ())?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerFromNib(ListCollectionCell.self)
        self.delegate = self
        self.dataSource = self
    }
    
    
    func update(items: [itemType]) {
        self.items = items
        self.reloadData()
    }
    
    

}

extension ListCollection: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ListCollectionCell = self.dequeueReusableCell(forIndexPath: indexPath)
        
        cell.setup((title: items[indexPath.row].title, image: items[indexPath.row].image))
        
        
        return cell
    }
    
    
    
}


extension ListCollection: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeOfTheItem = (UIScreen.main.bounds.width - 30 - 15) / 2
        return CGSize(width: sizeOfTheItem, height: sizeOfTheItem + 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.didSelect?(items[indexPath.row])
    }
}
