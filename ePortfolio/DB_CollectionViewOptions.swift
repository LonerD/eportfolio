//
//  DB_CollectionViewOptions.swift
//  eHealth
//
//  Created by Дмитрий Бондаренко on 17.01.17.
//  Copyright © 2017 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

protocol ReusableView: class {
    static var defaultReuseIdentifire: String { get }
        static var nibName: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifire: String {
        return NSStringFromClass(self)
    }
    
    static var nibName: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}

extension UICollectionReusableView: ReusableView { }

extension UICollectionView {
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView {
        register(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifire)
        
    }
    
    func registerFromNib<T: UICollectionViewCell>(_: T.Type) where T: ReusableView {
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifire)
    }
    
    
    
    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: T.defaultReuseIdentifire, for: indexPath) as? T else {
            fatalError()
        }
        return cell
    }
    
    func registerReusableView<T: UICollectionReusableView>(_: T.Type) where T:ReusableView {
        register(T.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: T.defaultReuseIdentifire)
    }
    
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(kind: String, at indexPath: IndexPath) -> T where T: ReusableView {
        guard let view = self.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: T.defaultReuseIdentifire, for: indexPath) as? T else {
            fatalError()
        }
        
        return view
    }
    
   
    
    
}
