//
//  StudentGroupNumberVC.swift
//  ePortfolio
//
//  Created by r00t on 31.03.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class StudentGroupNumberVC: UIViewController {

    @IBOutlet weak var tableView: TitledTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Група"
        let cources = ["1 група", "2 група", "3 група", "4 група", "5 група", "6 група", "7 група", "8 група", "9 група", "10 група", "11 група", "12 група"]
        
        self.tableView.setup(cources)
        self.tableView.didSelectWith = { _ in
            self.performSegue(withIdentifier: "to", sender: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
