//
//  ListCollectionCell.swift
//  ePortfolio
//
//  Created by r00t on 03.04.17.
//  Copyright © 2017 r00t. All rights reserved.
//

import UIKit

class ListCollectionCell: UICollectionViewCell {
    @IBOutlet weak var itemsImage: UIImageView!
    @IBOutlet weak var itemsLabel: UILabel!

    func setup(_ Item: (title: String, image: UIImage)) {
        self.itemsImage.image = Item.image
        self.itemsLabel.text = Item.title
    }

}
